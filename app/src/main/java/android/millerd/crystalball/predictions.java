package android.millerd.crystalball;

/**
 * Created by Student on 9/3/2015.
 */
public class predictions {

    private static predictions predictions;
    private String[] answers;

    private predictions() {
        answers = new String[] {
            "Your wishes might come true.",
            "Too bad sonny jim."
        };
    }

    public static predictions get() {
        if (predictions == null) {
            predictions = new predictions();
        }
        return predictions;
    }
    public String getprediction() {
        return answers[0];
    }
}
